version: '3.7'

services:
  nginx:
    image: linuxserver/letsencrypt
    ports:
      - '80:80'
      - '443:443'
    environment:
      PUID: "1000"
      PGID: "1000"
      URL: "${DOMAIN}"
      SUBDOMAINS: "sftp,files,grafana,influxdb,telegraf"
      ONLY_SUBDOMAINS: "true"
      VALIDATION: "http"
      EMAIL: "${LETSENCRYPT_ACCOUNT_EMAIL}"
      STAGING: "${STAGING}"
    configs:
      - source: nginx-telegraf-config
        target: /config/nginx/site-confs/telegraf.conf
      - source: nginx-stats-config
        target: /config/nginx/site-confs/nginx-stats.conf
      - source: nginx-grafana-config
        target: /config/nginx/proxy-confs/grafana.subdomain.conf
      - source: nginx-droppy-config
        target: /config/nginx/proxy-confs/droppy.subdomain.conf
    volumes:
      - ./www:/config/www
      - log-storage:/config/log
      - certs:/config/etc/letsencrypt

  smtp:
    image: namshi/smtp
    environment:
      - MAILNAME=${DOMAIN}
  droppy:
    image: "gdunstone/droppy:latest"
    depends_on:
      - nginx
    volumes:
      - ./www:/files
      - droppy-config-storage:/config
    environment:
      DROPPY_ADMIN_USER: ${DROPPY_ADMIN_USER}
      DROPPY_ADMIN_PASSWORD: ${DROPPY_ADMIN_PASSWORD}
  sftp:
    image: atmoz/sftp:alpine
    volumes:
      - ./www:/home
      - ./configs/sftp/users.conf:/etc/sftp/users.conf
    secrets:
      - source: sftp-ssh_host_ed25519_key
        target: /etc/ssh/ssh_host_ed25519_key
        mode: 0400
      - source: sftp-ssh_host_rsa_key
        target: /etc/ssh/ssh_host_rsa_key
        mode: 0400
    ports:
      - "2222:22"
  b2-uploader:
    image: registry.gitlab.com/gdunstone/b2-uploader:v0.0.3
    volumes:
      - ./www/upload/data:/data:rw
    environment:
      B2_ACCOUNT_ID: ${B2_ACCOUNT_ID}
      B2_APPLICATION_KEY: ${B2_APPLICATION_KEY}
      B2_BUCKET: ${B2_BUCKET}
  influxdb:
    image: influxdb:${INFLUXDB_VERSION}-alpine
    ports:
      - '8086:8086'
    volumes:
      - influxdb-storage:/var/lib/influxdb:rw
    environment:
      INFLUXDB_ADMIN_USER: "${INFLUXDB_ADMIN_USER}"
      INFLUXDB_ADMIN_PASSWORD: "${INFLUXDB_ADMIN_PASSWORD}"
      INFLUXDB_HTTP_AUTH_ENABLED: "true"
      INFLUXDB_LOGGING_LEVEL: "warn"
      INFLUXDB_DATA_QUERY_LOG_ENABLED: "true"
      INFLUXDB_HTTP_LOG_ENABLED: "true"
    secrets:
      - source: influxdb-init
        target: /docker-entrypoint-initdb.d/influxdb-init.iql

  kapacitor:
    image: "kapacitor:${KAPACITOR_VERSION}-alpine"
    depends_on:
      - influxdb
    volumes:
      - ./kapacitor:/etc/kapacitor/load/:ro
      - kapacitor-storage:/var/lib/kapacitor
    environment:
      KAPACITOR_HOSTNAME: kapacitor
      KAPACITOR_INFLUXDB_0_URLS_0: "http://influxdb:8086"
      KAPACITOR_INFLUXDB_0_USERNAME: "${INFLUXDB_ADMIN_USER}"
      KAPACITOR_INFLUXDB_0_PASSWORD: "${INFLUXDB_ADMIN_PASSWORD}"
      KAPACITOR_HTTP_LOG_ENABLED: "false"
      KAPACITOR_LOGGING_LEVEL: "INFO"
      KAPACITOR_SMTP_ENABLED: "true"
      KAPACITOR_SMTP_HOST: "smtp:25"
      KAPACITOR_SMTP_NO_VERIFY: "true"
      KAPACITOR_SMTP_FROM: "kapacitor-no-reply@${DOMAIN}"
      KAPACITOR_LOAD_ENABLED: "true"
      KAPACITOR_LOAD_DIR: /etc/kapacitor/load
      TZ: Australia/Canberra

  grafana:
    image: "grafana/grafana:${GRAFANA_VERSION}"
    depends_on:
      - influxdb
      - smtp
      - nginx
    volumes:
      - grafana-storage:/var/lib/grafana:rw
    secrets:
      - source: grafana-provisioning-init
        target: /etc/grafana/provisioning/datasources/datasources.yaml
    environment:
      GF_SECURITY_ADMIN_USER: "${GF_ADMIN_USER}"
      GF_SECURITY_ADMIN_PASSWORD: "${GF_ADMIN_PASSWORD}"
      GF_SECURITY_DATA_SOURCE_PROXY_WHITELIST: "influxdb:8086"
      GF_USERS_ALLOW_SIGN_UP: "false"
      GF_USERS_ALLOW_ORG_CREATE: "false"
      GF_AUTH_ANONYMOUS_ENABLED: "true"
      GF_AUTH_ANONYMOUS_ORG_NAME: "Public Org"
      GF_AUTH_ANONYMOUS_ORG_ROLE: "Viewer"
      GF_DEFAULT_INSTANCE_NAME: "grafana.${DOMAIN}"
      GF_SERVER_DOMAIN: "grafana.${DOMAIN}"
      GF_SERVER_ROOT_URL: "https://grafana.${DOMAIN}"
      GF_SMTP_ENABLED: "true"
      GF_SMTP_HOST: "smtp:25"
      GF_SMTP_SKIP_VERIFY: "true"
      GF_SMTP_FROM_ADDRESS: "grafana-no-reply@${DOMAIN}"
      GF_INSTALL_PLUGINS: "ryantxu-ajax-panel,farski-blendstat-panel,pr0ps-trackmap-panel,snuids-radar-panel,raintank-worldping-app,ryantxu-ajax-panel,fetzerch-sunandmoon-datasource,andig-darksky-datasource,grafana-influxdb-flux-datasource"
      TZ: Australia/Canberra
  telegraf-mon:
    image: "telegraf:${TELEGRAF_VERSION}-alpine"
    depends_on:
      - influxdb
    command: --config=https://telegraf.${DOMAIN}/telegraf-mon.toml
    environment:
      DOMAIN_TAG: ${DOMAIN}
      INFLUXDB_URL: http://influxdb:8086
      INFLUXDB_DATABASE: ${INFLUXDB_MON_DB}
      INFLUXDB_USER: ${INFLUXDB_MON_WRITER}
      INFLUXDB_PASSWORD: ${INFLUXDB_MON_WRITER_PASSWORD}
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - log-storage:/var/log/
  telegraf-ttn:
    image: "telegraf:${TELEGRAF_VERSION}-alpine"
    depends_on:
      - influxdb
    command: --config=https://telegraf.${DOMAIN}/telegraf-ttn.toml
    environment:
      INFLUXDB_URL: http://influxdb:8086
      INFLUXDB_DATABASE: ${INFLUXDB_TTN_DB}
      INFLUXDB_USER: ${INFLUXDB_TTN_WRITER}
      INFLUXDB_PASSWORD: ${INFLUXDB_TTN_WRITER_PASSWORD}

volumes:
  grafana-storage: {}
  influxdb-storage: {}
  kapacitor-storage: {}
  droppy-config-storage: {}
  certs: {}
  log-storage: {}

configs:
  nginx-telegraf-config:
    file: ./configs/nginx/site-confs/telegraf.conf
  nginx-stats-config:
    file: ./configs/nginx/site-confs/nginx-stats.conf
  nginx-grafana-config:
    file: ./configs/nginx/proxy-confs/grafana.subdomain.conf
  nginx-droppy-config:
    file: ./configs/nginx/proxy-confs/droppy.subdomain.conf

secrets:
  influxdb-init:
    file: ./secrets/influxdb/influxdb-init.iql
  grafana-provisioning-init:
    file: ./secrets/grafana/datasources.yaml
  sftp-ssh_host_ed25519_key:
    file: ./secrets/sftp/keys/ssh_host_ed25519_key
  sftp-ssh_host_rsa_key:
    file: ./secrets/sftp/keys/ssh_host_rsa_key
