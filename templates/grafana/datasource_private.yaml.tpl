  - name: \"InfluxDB ${DB}\"
    orgId: 1
    type: influxdb
    url: ${INFLUXDB_URL}
    user: ${USER}
    access: proxy
    database: ${DB}
    isDefault: false
    editable: true
    secureJsonData:
      password: ${PASSWORD}
