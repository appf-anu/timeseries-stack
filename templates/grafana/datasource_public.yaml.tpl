  - name: \"InfluxDB ${DB}\"
    orgId: 1
    type: influxdb
    url: ${INFLUXDB_URL}
    user: ${INFLUXDB_PUBLIC_USER}
    access: proxy
    database: ${DB}
    isDefault: ${IS_DEFAULT}
    editable: false
    secureJsonData:
      password: ''
