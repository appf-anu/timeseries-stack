#!/bin/bash
yum -y update
packages=$(cat <<- EOM
zsh
curl
git
jq
pcre-tools
aspell-en
openssl
yum-utils 
device-mapper-persistent-data
lvm2
vim-common
EOM
)
yum -y install ${packages}

yum-config-manager -y --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce
usermod -aG docker $(whoami)
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
systemctl start docker.service
systemctl enable docker.service
aspell -d british dump master >> /usr/share/dict/british-english

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/bhilburn/powerlevel9k.git ~/.powerlevel9k || true

curl -L https://ftp.gnu.org/gnu/aspell/dict/en/aspell6-en-2019.10.06-0.tar.bz2 
cat > ~/.zshrc << EOM
export ZSH="/root/.oh-my-zsh"
plugins=(git docker docker-compose)
source $ZSH/oh-my-zsh.sh
POWERLEVEL9K_MODE='nerdfont-complete'
source  ~/.powerlevel9k/powerlevel9k.zsh-theme
EOM

git clone https://gitlab.com/appf-anu/timeseries-stack.git ~/timeseries-stack