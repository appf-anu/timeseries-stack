#!/bin/bash
rm config.env
rm docker-compose.yml
rm secrets/grafana/datasources.yaml
rm secrets/influxdb/influxdb-init.iql
rm secrets/sftp/keys/*
rm secrets/sftp/*
